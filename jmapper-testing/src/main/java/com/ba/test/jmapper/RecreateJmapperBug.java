package com.ba.test.jmapper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by n448072 on 03/08/2015.
 */
public class RecreateJmapperBug {

    private final static String TEMPLATE = "   $newInstance(destination)\n" +
            "   Object[] collectionOfSource1 = $getSource().toArray();\n" +
            "   for(int $i = 0;$i<collectionOfSource1.length;$i++){\n" +
            "   $sClass objectOfSoure0 = ($sClass) collectionOfSource1[$i];\n" +
            "$mapping\n" +
            "   $destination.add($dItem);\n" +
            "   }\n" +
            "   objectOfDestination0.setFlightLeg(collectionOfDestination0);\n";

    private final static String REPLACE = "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.FlightLeg objectOfDestination0 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.FlightLeg();\n" +
            "   if(objectOfSoure0.getRepeatNumber()!=null){\n" +
            "   objectOfDestination0.setRepeatNumber(objectOfSoure0.getRepeatNumber());\n" +
            "   }\n" +
            "   if(objectOfSoure0.getUpdateDateTime()!=null){\n" +
            "   objectOfDestination0.setUpdateDateTime(objectOfSoure0.getUpdateDateTime());\n" +
            "   }\n" +
            "   if(objectOfSoure0.getCurrentOrigin()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Movement obj1 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Movement();\n" +
            "   obj1.setSequenceNumber(objectOfSoure0.getCurrentOrigin().getSequenceNumber());\n" +
            "   obj1.setLocalDateTime(objectOfSoure0.getCurrentOrigin().getLocalDateTime());\n" +
            "   if(objectOfSoure0.getCurrentOrigin().getAirport()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Airport obj2 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Airport();\n" +
            "   obj2.setAirportCode(objectOfSoure0.getCurrentOrigin().getAirport().getAirportCode());\n" +
            "   obj2.setAirportName(objectOfSoure0.getCurrentOrigin().getAirport().getAirportName());\n" +
            "   obj1.setAirport(obj2);\n" +
            "   }else{\n" +
            "   obj1.setAirport(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getCurrentOrigin().getCity()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.City obj3 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.City();\n" +
            "   obj3.setCityCode(objectOfSoure0.getCurrentOrigin().getCity().getCityCode());\n" +
            "   obj3.setCityName(objectOfSoure0.getCurrentOrigin().getCity().getCityName());\n" +
            "   obj1.setCity(obj3);\n" +
            "   }else{\n" +
            "   obj1.setCity(null);\n" +
            "   }\n" +
            "   objectOfDestination0.setCurrentOrigin(obj1);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getCurrentDestination()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Movement obj4 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Movement();\n" +
            "   obj4.setSequenceNumber(objectOfSoure0.getCurrentDestination().getSequenceNumber());\n" +
            "   obj4.setLocalDateTime(objectOfSoure0.getCurrentDestination().getLocalDateTime());\n" +
            "   if(objectOfSoure0.getCurrentDestination().getAirport()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Airport obj5 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Airport();\n" +
            "   obj5.setAirportCode(objectOfSoure0.getCurrentDestination().getAirport().getAirportCode());\n" +
            "   obj5.setAirportName(objectOfSoure0.getCurrentDestination().getAirport().getAirportName());\n" +
            "   obj4.setAirport(obj5);\n" +
            "   }else{\n" +
            "   obj4.setAirport(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getCurrentDestination().getCity()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.City obj6 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.City();\n" +
            "   obj6.setCityCode(objectOfSoure0.getCurrentDestination().getCity().getCityCode());\n" +
            "   obj6.setCityName(objectOfSoure0.getCurrentDestination().getCity().getCityName());\n" +
            "   obj4.setCity(obj6);\n" +
            "   }else{\n" +
            "   obj4.setCity(null);\n" +
            "   }\n" +
            "   objectOfDestination0.setCurrentDestination(obj4);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getLegTimings()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.FlightTimings obj7 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.FlightTimings();\n" +
            "   if(objectOfSoure0.getLegTimings().getScheduledDepartureDateTime()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate obj8 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate();\n" +
            "   obj8.setLocal(objectOfSoure0.getLegTimings().getScheduledDepartureDateTime().getLocal());\n" +
            "   obj8.setUtc(objectOfSoure0.getLegTimings().getScheduledDepartureDateTime().getUtc());\n" +
            "   obj7.setScheduledDepartureDateTime(obj8);\n" +
            "   }else{\n" +
            "   obj7.setScheduledDepartureDateTime(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getLegTimings().getScheduledArrivalDateTime()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate obj9 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate();\n" +
            "   obj9.setLocal(objectOfSoure0.getLegTimings().getScheduledArrivalDateTime().getLocal());\n" +
            "   obj9.setUtc(objectOfSoure0.getLegTimings().getScheduledArrivalDateTime().getUtc());\n" +
            "   obj7.setScheduledArrivalDateTime(obj9);\n" +
            "   }else{\n" +
            "   obj7.setScheduledArrivalDateTime(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getLegTimings().getEstimatedDepartureDateTime()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate obj10 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate();\n" +
            "   obj10.setLocal(objectOfSoure0.getLegTimings().getEstimatedDepartureDateTime().getLocal());\n" +
            "   obj10.setUtc(objectOfSoure0.getLegTimings().getEstimatedDepartureDateTime().getUtc());\n" +
            "   obj7.setEstimatedDepartureDateTime(obj10);\n" +
            "   }else{\n" +
            "   obj7.setEstimatedDepartureDateTime(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getLegTimings().getEstimatedArrivalDateTime()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate obj11 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate();\n" +
            "   obj11.setLocal(objectOfSoure0.getLegTimings().getEstimatedArrivalDateTime().getLocal());\n" +
            "   obj11.setUtc(objectOfSoure0.getLegTimings().getEstimatedArrivalDateTime().getUtc());\n" +
            "   obj7.setEstimatedArrivalDateTime(obj11);\n" +
            "   }else{\n" +
            "   obj7.setEstimatedArrivalDateTime(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getLegTimings().getActualDepartureDateTime()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate obj12 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate();\n" +
            "   obj12.setLocal(objectOfSoure0.getLegTimings().getActualDepartureDateTime().getLocal());\n" +
            "   obj12.setUtc(objectOfSoure0.getLegTimings().getActualDepartureDateTime().getUtc());\n" +
            "   obj7.setActualDepartureDateTime(obj12);\n" +
            "   }else{\n" +
            "   obj7.setActualDepartureDateTime(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getLegTimings().getActualArrivalDateTime()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate obj13 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.EventDate();\n" +
            "   obj13.setLocal(objectOfSoure0.getLegTimings().getActualArrivalDateTime().getLocal());\n" +
            "   obj13.setUtc(objectOfSoure0.getLegTimings().getActualArrivalDateTime().getUtc());\n" +
            "   obj7.setActualArrivalDateTime(obj13);\n" +
            "   }else{\n" +
            "   obj7.setActualArrivalDateTime(null);\n" +
            "   }\n" +
            "   objectOfDestination0.setLegTimings(obj7);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getAircraftForFlightLeg()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.AircraftForFlightLeg obj14 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.AircraftForFlightLeg();\n" +
            "   if(objectOfSoure0.getAircraftForFlightLeg().getAircraft()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Aircraft obj15 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Aircraft();\n" +
            "   obj15.setAircraftRegistration(objectOfSoure0.getAircraftForFlightLeg().getAircraft().getAircraftRegistration());\n" +
            "   if(objectOfSoure0.getAircraftForFlightLeg().getAircraft().getOperator()!=null){\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Organisation obj16 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.Organisation();\n" +
            "   obj16.setName(objectOfSoure0.getAircraftForFlightLeg().getAircraft().getOperator().getName());\n" +
            "   obj16.setCode(objectOfSoure0.getAircraftForFlightLeg().getAircraft().getOperator().getCode());\n" +
            "   obj15.setOperator(obj16);\n" +
            "   }else{\n" +
            "   obj15.setOperator(null);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getAircraftForFlightLeg().getAircraft().getAircraftType()!=null){\n" +
            "   java.util.ArrayList collectionOfDestination0 = new java.util.ArrayList();\n" +
            "   Object[] collectionOfSource0 = objectOfSoure0.getAircraftForFlightLeg().getAircraft().getAircraftType().toArray();\n" +
            "   for(int index0 = 0;index0<collectionOfSource0.length;index0++){\n" +
            "   com.ba.schema.fdm.flightmanagerv02.getaircraftlinkedflightsresponsev01.AircraftType objectOfSoure0 = (com.ba.schema.fdm.flightmanagerv02.getaircraftlinkedflightsresponsev01.AircraftType) collectionOfSource0[index0];\n" +
            "   com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.AircraftType objectOfDestination0 = new com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.AircraftType();\n" +
            "   objectOfDestination0.setAircraftType(objectOfSoure0.getAircraftType());\n" +
            "   objectOfDestination0.setAircraftTypeDescription(objectOfSoure0.getAircraftTypeDescription());\n" +
            "   objectOfDestination0.setAircraftTypeSource(objectOfSoure0.getAircraftTypeSource());\n" +
            "   objectOfDestination0.setAircraftSubType(objectOfSoure0.getAircraftSubType());\n" +
            "\n" +
            "   collectionOfDestination0.add(objectOfDestination0);\n" +
            "   }\n" +
            "   obj15.setAircraftType(collectionOfDestination0);\n" +
            "\n" +
            "   }else{\n" +
            "   obj15.setAircraftType(null);\n" +
            "   }\n" +
            "   obj14.setAircraft(obj15);\n" +
            "   }else{\n" +
            "   obj14.setAircraft(null);\n" +
            "   }\n" +
            "   objectOfDestination0.setAircraftForFlightLeg(obj14);\n" +
            "   }\n" +
            "   if(objectOfSoure0.getLegDuration()!=null){\n" +
            "   FlightLeg$durationConversion(objectOfDestination0.getLegDuration(), objectOfSoure0.getLegDuration());\n" +
            "   }\n";

    public static void main(String[] args) {
        String template = new String(TEMPLATE);
        String replace = new String(REPLACE);
        //replace = replace.replaceAll("\\$", Matcher.quoteReplacement("\\$"));
        String key = "$mapping";
        //template = template.replaceAll(Pattern.quote(key), replace.replaceAll("\\$", Matcher.quoteReplacement("\\$")));
        template = template.replaceAll(Pattern.quote(key), Matcher.quoteReplacement(replace));
        System.out.println(template);
    }
}
