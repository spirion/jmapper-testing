package com.ba.test.jmapper;

/**
 * Enumeration details test run type.
 *
 * @author Micvhael Conway
 */
public enum TestType {
    JMAPPER, MANUAL;
}
