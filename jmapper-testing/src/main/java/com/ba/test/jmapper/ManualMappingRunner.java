package com.ba.test.jmapper;

/**
 * Used to run the manual mapping test.
 */
public class ManualMappingRunner {

    /**
     * Number of iterations for the mapping test.
     */
    private static final int ITERATIONS = 1000000;

    /**
     * Run the manual mapping tests.
     *
     * @param args Arguments
     */
    public static void main(String[] args) {
        System.out.println("**** TEST STARTED ****");
        final ClassMappingDelegate classMappingDelegate = new ClassMappingDelegate();
        warmUp(classMappingDelegate);
        runTest(classMappingDelegate);
    }

    /**
     * A quick warm up cycle.
     *
     * @param classMappingDelegate The class mapping delegate bean
     */
    private static void warmUp(final ClassMappingDelegate classMappingDelegate) {
        System.out.println("**** WARMING UP ****");
        classMappingDelegate.runMappingTest(10, TestType.MANUAL);
    }

    /**
     * Run the test.
     *
     * @param classMappingDelegate The class mapping delegate bean.
     */
    private static void runTest(final ClassMappingDelegate classMappingDelegate) {
        System.out.println("**** STARTING TEST FOR " + ITERATIONS + " ITERATIONS ****");
        long start = System.currentTimeMillis();
        classMappingDelegate.runMappingTest(ITERATIONS, TestType.MANUAL);
        long finish = System.currentTimeMillis();
        System.out.print("**** Total time for " + ITERATIONS + " iterations : " + (finish - start) + "ms ****");
    }

}
