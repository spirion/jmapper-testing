package com.ba.test.jmapper;

import com.ba.captwo.pegasus.pega.coreservice.converters.FMGGALFResponseConverter;
import com.ba.schema.fdm.flightmanagerv02.getaircraftlinkedflightsresponsev01.GetAircraftLinkedFlightsResponse;
import com.googlecode.jmapper.JMapper;
import com.googlecode.jmapper.enums.ChooseConfig;

/**
 * Responsible for running multiple instances of mapping tests for either manual mapping or jmapper mapping.
 *
 * @author Michael.Conway
 */
public class ClassMappingDelegate {

    /**
     * The bean to convert.
     */
    private GetAircraftLinkedFlightsResponse getAircraftLinkedFlightsResponse;

    /**
     * The manual converter bean.
     */
    private FMGGALFResponseConverter fmggalfResponseConverter;

    /**
     * The jmapper instance.
     */
    private JMapper<com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.GetAircraftLinkedFlightsResponse, GetAircraftLinkedFlightsResponse> jmapper;

    /**
     * Constructor.
     */
    public ClassMappingDelegate() {
        final GetAircraftLinkedFlightsResponseCreationDelegate getAircraftLinkedFlightsResponseCreationDelegate = new GetAircraftLinkedFlightsResponseCreationDelegate();
        this.getAircraftLinkedFlightsResponse = getAircraftLinkedFlightsResponseCreationDelegate.create();
        this.fmggalfResponseConverter = new FMGGALFResponseConverter();
        configureJmapper();
    }

    /**
     * Configure jmapper.
     */
    private void configureJmapper() {
        final String jmapperXmlLocation = "file:C:/Users/n448072/git/jmapper-testing/jmapper-testing/jmapper-testing/src/main/java/jmapper.xml";
        this.jmapper = new JMapper<>(
                com.ba.schema.pega.pegasusadapterv02.getaircraftlinkedflightsresponsev01.GetAircraftLinkedFlightsResponse.class,
                GetAircraftLinkedFlightsResponse.class, ChooseConfig.DESTINATION, jmapperXmlLocation);
    }

    /**
     * Run a mapping process a number of times.
     *
     * @param iterations Number of iterations
     * @param testType   Type of test to run
     */
    public void runMappingTest(int iterations, final TestType testType) {
        if (testType == TestType.JMAPPER) {
            doJmapperProcess(iterations);
        } else if (testType == TestType.MANUAL) {
            doManualProcess(iterations);
        }
    }

    /**
     * Perform the jmapper process a number of times.
     *
     * @param iterations Number of iterations
     */
    private void doJmapperProcess(int iterations) {
        for (int index = 0; index < iterations; index++) {
            Object o = jmapper.getDestination(this.getAircraftLinkedFlightsResponse);
            int x = 1;
        }
    }

    /**
     * Do the manual mapping process a number of times.
     *
     * @param iterations Number of iterations
     */
    private void doManualProcess(int iterations) {
        for (int index = 0; index < iterations; index++) {
            fmggalfResponseConverter.convert(this.getAircraftLinkedFlightsResponse);
        }
    }

}
